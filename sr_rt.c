/*-----------------------------------------------------------------------------
 * file:  sr_rt.c
 * date:  Mon Oct 07 04:02:12 PDT 2002
 * Author:  casado@stanford.edu
 *
 * Description:
 *
 *---------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>


#include <sys/socket.h>
#include <netinet/in.h>
#define __USE_MISC 1 /* force linux to show inet_aton */
#include <arpa/inet.h>

#include "sr_rt.h"
#include "sr_if.h"
#include "sr_utils.h"
#include "sr_router.h"

/*---------------------------------------------------------------------
 * Method:
 *
 *---------------------------------------------------------------------*/

int sr_load_rt(struct sr_instance* sr,const char* filename)
{
    FILE* fp;
    char  line[BUFSIZ];
    char  dest[32];
    char  gw[32];
    char  mask[32];    
    char  iface[32];
    struct in_addr dest_addr;
    struct in_addr gw_addr;
    struct in_addr mask_addr;
    int clear_routing_table = 0;

    /* -- REQUIRES -- */
    assert(filename);
    if( access(filename,R_OK) != 0)
    {
        perror("access");
        return -1;
    }

    fp = fopen(filename,"r");

    while( fgets(line,BUFSIZ,fp) != 0)
    {
        sscanf(line,"%s %s %s %s",dest,gw,mask,iface);
        if(inet_aton(dest,&dest_addr) == 0)
        { 
            fprintf(stderr,
                    "Error loading routing table, cannot convert %s to valid IP\n",
                    dest);
            return -1; 
        }
        if(inet_aton(gw,&gw_addr) == 0)
        { 
            fprintf(stderr,
                    "Error loading routing table, cannot convert %s to valid IP\n",
                    gw);
            return -1; 
        }
        if(inet_aton(mask,&mask_addr) == 0)
        { 
            fprintf(stderr,
                    "Error loading routing table, cannot convert %s to valid IP\n",
                    mask);
            return -1; 
        }
        if( clear_routing_table == 0 ){
            printf("Loading routing table from server, clear local routing table.\n");
            sr->routing_table = 0;
            clear_routing_table = 1;
        }
        sr_add_rt_entry(sr,dest_addr,gw_addr,mask_addr,(uint32_t)0,iface);
    } /* -- while -- */

    return 0; /* -- success -- */
} /* -- sr_load_rt -- */

/*---------------------------------------------------------------------
 * Method:
 *
 *---------------------------------------------------------------------*/
int sr_build_rt(struct sr_instance* sr){
    struct sr_if* interface = sr->if_list;
    char  iface[32];
    struct in_addr dest_addr;
    struct in_addr gw_addr;
    struct in_addr mask_addr;

    while (interface){
        dest_addr.s_addr = (interface->ip & interface->mask);
        gw_addr.s_addr = 0;
        mask_addr.s_addr = interface->mask;
        strcpy(iface, interface->name);
        sr_add_rt_entry(sr, dest_addr, gw_addr, mask_addr, (uint32_t)0, iface);
        interface = interface->next;
    }
    return 0;
}

void sr_add_rt_entry(struct sr_instance* sr, struct in_addr dest,
struct in_addr gw, struct in_addr mask, uint32_t metric, char* if_name)
{   
    struct sr_rt* rt_walker = 0;

    /* -- REQUIRES -- */
    assert(if_name);
    assert(sr);

    pthread_mutex_lock(&(sr->rt_lock));
    /* -- empty list special case -- */
    if(sr->routing_table == 0)
    {
        sr->routing_table = (struct sr_rt*)malloc(sizeof(struct sr_rt));
        assert(sr->routing_table);
        sr->routing_table->next = 0;
        sr->routing_table->dest = dest;
        sr->routing_table->gw   = gw;
        sr->routing_table->mask = mask;
        strncpy(sr->routing_table->interface,if_name,sr_IFACE_NAMELEN);
        sr->routing_table->metric = metric;
        time_t now;
        time(&now);
        sr->routing_table->updated_time = now;

        pthread_mutex_unlock(&(sr->rt_lock));
        return;
    }

    /* -- find the end of the list -- */
    rt_walker = sr->routing_table;
    while(rt_walker->next){
      rt_walker = rt_walker->next; 
    }

    rt_walker->next = (struct sr_rt*)malloc(sizeof(struct sr_rt));
    assert(rt_walker->next);
    rt_walker = rt_walker->next;

    rt_walker->next = 0;
    rt_walker->dest = dest;
    rt_walker->gw   = gw;
    rt_walker->mask = mask;
    strncpy(rt_walker->interface,if_name,sr_IFACE_NAMELEN);
    rt_walker->metric = metric;
    time_t now;
    time(&now);
    rt_walker->updated_time = now;
    
     pthread_mutex_unlock(&(sr->rt_lock));
} /* -- sr_add_entry -- */

/*---------------------------------------------------------------------
 * Method:
 *
 *---------------------------------------------------------------------*/

void sr_print_routing_table(struct sr_instance* sr)
{
    pthread_mutex_lock(&(sr->rt_lock));
    struct sr_rt* rt_walker = 0;

    if(sr->routing_table == 0)
    {
        printf(" *warning* Routing table empty \n");
        pthread_mutex_unlock(&(sr->rt_lock));
        return;
    }
    printf("  <---------- Router Table ---------->\n");
    printf("Destination\tGateway\t\tMask\t\tIface\tMetric\tUpdate_Time\n");

    rt_walker = sr->routing_table;
    
    while(rt_walker){
        if (rt_walker->metric < INFINITY)
            sr_print_routing_entry(rt_walker);
        rt_walker = rt_walker->next;
    }
    pthread_mutex_unlock(&(sr->rt_lock));


} /* -- sr_print_routing_table -- */

/*---------------------------------------------------------------------
 * Method:
 *
 *---------------------------------------------------------------------*/

void sr_print_routing_entry(struct sr_rt* entry)
{
    /* -- REQUIRES --*/
    assert(entry);
    assert(entry->interface);
    
    char buff[20];
    struct tm* timenow = localtime(&(entry->updated_time));
    strftime(buff, sizeof(buff), "%H:%M:%S", timenow);
    printf("%s\t",inet_ntoa(entry->dest));
    printf("%s\t",inet_ntoa(entry->gw));
    printf("%s\t",inet_ntoa(entry->mask));
    printf("%s\t",entry->interface);
    printf("%d\t",entry->metric);
    printf("%s\n", buff);

} /* -- sr_print_routing_entry -- */


void *sr_rip_timeout(void *sr_ptr) {
	
    struct sr_instance *sr = sr_ptr;
    while (1) {
        sleep(5); /*delay */
        pthread_mutex_lock(&(sr->rt_lock));
		
		/* check if in interface list - if not: expired */
		struct sr_if* interface_list = sr->if_list;
		
		while (interface_list != NULL) {
			if (sr_obtain_interface_status(sr, interface_list->name) == 0) {
				struct sr_rt* route_list = sr->routing_table;
				while (route_list) {
					int string_comp = strcmp(route_list->interface, interface_list->name);
					if (string_comp==0) {
						route_list->metric = htons(INFINITY);
					}
					route_list = route_list->next;
				}
			}
			interface_list = interface_list->next;
		}
		
		time_t time_this_iteration;
		time(&time_this_iteration);
		
		struct sr_rt* route_list = 0;
		route_list = sr->routing_table;
		while (route_list->next) {
			if ((route_list->next)->updated_time - time_this_iteration > 20) {
				/* entry expired !!!!!*/
				route_list->next = (route_list->next)->next;
			} else {
				route_list = route_list->next;
			}
		}
		send_rip_response(sr);
        pthread_mutex_unlock(&(sr->rt_lock));
    }
    return NULL;
}

void send_rip_request(struct sr_instance *sr){
    /* Fill your code here */
	
	/* malloc packet */
	uint8_t* pac = (uint8_t*) malloc(sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t) + sizeof(sr_udp_hdr_t) + sizeof(sr_rip_pkt_t));
	
	/* INITIALIZE ALL HEADERS */
	sr_ip_hdr_t* ip_hdr = (sr_ip_hdr_t*) (pac + sizeof(sr_ethernet_hdr_t));
	ip_hdr->ip_ttl = 64;
	ip_hdr->ip_p = ip_protocol_udp;
	
	sr_udp_hdr_t* udp_hdr = (sr_udp_hdr_t*) (pac + sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t));
	udp_hdr->port_src = htons(520);
	udp_hdr->port_dst = htons(520);
	udp_hdr->udp_len = htons(sizeof(sr_udp_hdr_t) + sizeof(sr_rip_pkt_t));
	udp_hdr->udp_sum = 0;
	udp_hdr->udp_sum = cksum(udp_hdr, udp_hdr->udp_len);
	
	ip_hdr->ip_dst = ~(0x0);
	ip_hdr->ip_v = 4;
	ip_hdr->ip_hl = 5;
	ip_hdr->ip_off = htons(IP_DF);
	ip_hdr->ip_tos = 0;
	ip_hdr->ip_len = htons(sizeof(sr_ip_hdr_t) + sizeof(sr_udp_hdr_t) + sizeof(sr_rip_pkt_t));
	
	sr_rip_pkt_t* rip_hdr = (sr_rip_pkt_t*) (pac + sizeof(sr_ip_hdr_t) + sizeof(sr_ethernet_hdr_t) + sizeof(sr_udp_hdr_t));
	rip_hdr->command = 1;
	rip_hdr->version = 2;

	sr_ethernet_hdr_t* ether_hdr = (sr_ethernet_hdr_t*) pac;
	ether_hdr->ether_type = htons(ethertype_ip);
	memset(ether_hdr->ether_dhost, 0xff, ETHER_ADDR_LEN);
	
	
	struct sr_if* interface_in_list =0;
	interface_in_list = sr->if_list;
	
	while (interface_in_list) {
		ip_hdr->ip_src = interface_in_list->ip;
		memcpy(ether_hdr->ether_shost, interface_in_list->addr, ETHER_ADDR_LEN);
		/*check sum*/
		ip_hdr->ip_sum = 0;
		ip_hdr->ip_sum = cksum(ip_hdr, sizeof(sr_ip_hdr_t));

		print_hdrs(pac, sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t) + sizeof(sr_udp_hdr_t) + sizeof(sr_rip_pkt_t));
		if ((sr_send_packet(sr, pac, sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t) + sizeof(sr_udp_hdr_t) + sizeof(sr_rip_pkt_t), interface_in_list->name)) != 0) {
			printf("Error!");
		} else {
			printf("sent");
		}
		interface_in_list = interface_in_list->next;
	}
	free(pac); /*dont forget to free */
	
}

void send_rip_response(struct sr_instance *sr) {
	
	/* Fill your code here */
		
	struct sr_if* interface_in_list =0;
	interface_in_list = sr->if_list;
	while (interface_in_list) {
		uint8_t* pac = (uint8_t*) malloc(sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t) + sizeof(sr_udp_hdr_t) + sizeof(sr_rip_pkt_t));
		
		sr_rip_pkt_t* rip_head = (sr_rip_pkt_t*) (pac + sizeof(sr_ip_hdr_t) + sizeof(sr_ethernet_hdr_t) + sizeof(sr_udp_hdr_t));
		rip_head->command = 2;
		rip_head->version = 2;
		
		sr_print_routing_table(sr);
		pthread_mutex_lock(&(sr->rt_lock));
		struct sr_rt* route_list = 0;
		
		route_list = sr->routing_table;
		int x;
		for (x = 0; x < 25; x++) {
			if (route_list != NULL) {
				(rip_head->entries[x]).address = route_list->dest.s_addr;
				(rip_head->entries[x]).mask = route_list->mask.s_addr;
				/* split horizon */
				int string_comp = strcmp(route_list->interface, interface_in_list->name);
				if (string_comp==0) {
					(rip_head->entries[x]).metric = htons(INFINITY);
				} else {
					(rip_head->entries[x]).metric = route_list->metric;
				}
				(rip_head->entries[x]).next_hop = route_list->gw.s_addr;
				route_list = route_list->next;
			} else {
				(rip_head->entries[x]).metric = htons(INFINITY);
				(rip_head->entries[x]).next_hop = 0x0;
				(rip_head->entries[x]).address = 0x0;
				(rip_head->entries[x]).mask = 0x0;
			}
		}
		pthread_mutex_unlock(&(sr->rt_lock));
		/* udp headers now */
		sr_udp_hdr_t* udp_head = (sr_udp_hdr_t*) (pac + sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t));
		/* check sum first */
		udp_head->udp_sum = 0;
		udp_head->udp_sum = cksum(udp_head, udp_head->udp_len);

		udp_head->port_src = htons(520);
		udp_head->port_dst = htons(520);
		udp_head->udp_len = htons(sizeof(sr_udp_hdr_t) + sizeof(sr_rip_pkt_t));

		sr_ethernet_hdr_t* ether_head = (sr_ethernet_hdr_t*) pac;
		ether_head->ether_type = htons(ethertype_ip);
		memset(ether_head->ether_dhost, 0xff, ETHER_ADDR_LEN);

		sr_ip_hdr_t* ip_head = (sr_ip_hdr_t*) (pac + sizeof(sr_ethernet_hdr_t));
		ip_head->ip_tos = 0;
		ip_head->ip_len = htons(sizeof(sr_ip_hdr_t) + sizeof(sr_udp_hdr_t) + sizeof(sr_rip_pkt_t));
		ip_head->ip_ttl = 64;
		ip_head->ip_p = ip_protocol_udp;
		ip_head->ip_v = 4;
		ip_head->ip_hl = 5;
		ip_head->ip_dst = ~(0x0);
		ip_head->ip_off = htons(IP_DF);
		
			
		memcpy(ether_head->ether_shost, interface_in_list->addr, ETHER_ADDR_LEN);
		ip_head->ip_src = interface_in_list->ip;
		ip_head->ip_sum = 0;
		ip_head->ip_sum = cksum(ip_head, sizeof(sr_ip_hdr_t));
			if ((sr_send_packet(sr, pac, sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t) + sizeof(sr_udp_hdr_t) + sizeof(sr_rip_pkt_t), interface_in_list->name)) != 0) {
				printf("Error in sending RIP response.");
			} else {
				printf("RIP response sent.");
			}
		
		
		free(pac);
		interface_in_list = interface_in_list->next;
	}
	
}

void update_route_table(struct sr_instance *sr, sr_ip_hdr_t* ip_packet, sr_rip_pkt_t* rip_packet, char* iface){
    pthread_mutex_lock(&(sr->rt_lock));
    /* Fill your code here */
    int changes = 0;
    sr_print_routing_table(sr);
    struct sr_rt* entry = sr->routing_table;
    int found_entry = 0;
    int i;
    for (i = 0; i < MAX_NUM_ENTRIES; i++) {
        struct entry *curr_entry = &(rip_packet->entries[i]);
        found_entry = 0;
        entry = sr->routing_table;
        
        while (entry && (found_entry == 0)) {

            if (curr_entry->address == entry->dest.s_addr) {
            	
            	/* if curr = 0, check interface status */
            	if (entry->gw.s_addr == 0x0) {
            		int status = sr_obtain_interface_status(sr, entry->interface);
            		if ((status)!=0) {
            			entry->metric = 0;
            		}
            	} else {
            	
            	/* update time */
            	time_t new_time;
            	time(&new_time);
            	entry->updated_time = new_time;
            	/*if metric of the rip is lower then update values*/
                if ((curr_entry->metric+1) < entry->metric) {
                    entry->metric = curr_entry->metric+1;
                    strncpy(entry->interface,iface,sr_IFACE_NAMELEN);
                    entry->gw.s_addr = ip_packet->ip_src;
                    changes = 1;
                }
            	}
                found_entry = 1;
            }
        	/* iterate */
            entry = entry->next;
        }
        if (!found_entry) {
        	/* must add a new entry */
           struct in_addr new_address;
           new_address.s_addr = curr_entry->address;
           struct in_addr new_gw;
           new_gw.s_addr = ip_packet->ip_src;
           struct in_addr add_new_mask;
           add_new_mask.s_addr = curr_entry->mask;
           sr_add_rt_entry(sr, new_address, new_gw, add_new_mask, curr_entry->metric+1, iface);
           
           changes = 1;
        }
    }
    if (changes) {
    	send_rip_response(sr);
    }
    pthread_mutex_unlock(&(sr->rt_lock));
}

void send_rip_update(struct sr_instance *sr){
    pthread_mutex_lock(&(sr->rt_lock));
    /* Fill your code here */

    pthread_mutex_unlock(&(sr->rt_lock));
}


