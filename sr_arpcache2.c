#include <netinet/in.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include <sched.h>
#include <string.h>
#include "sr_arpcache.h"
#include "sr_router.h"
#include "sr_if.h"
#include "sr_protocol.h"
#include "sr_rt.h"
#include "sr_utils.h"

/* 
  This function gets called every second. For each request sent out, we keep
  checking whether we should resend an request or destroy the arp request.
  See the comments in the header file for an idea of what it should look like.
*/

void send_icmp (struct sr_instance* sr, uint8_t* packet, struct sr_if* receiving_if, sr_ethernet_hdr_t* eth_head, sr_ip_hdr_t* ip_header, uint8_t type, uint8_t code, char* interface) {
    /* make ether header */
    sr_ethernet_hdr_t* temp_eth_head = malloc(sizeof(sr_ethernet_hdr_t));
    memcpy(temp_eth_head->ether_shost, receiving_if->addr, ETHER_ADDR_LEN);
    memcpy(temp_eth_head->ether_dhost, eth_head->ether_shost, ETHER_ADDR_LEN);
    temp_eth_head->ether_type = (uint16_t)htons(ethertype_ip);
    memcpy(packet, temp_eth_head, sizeof(sr_ethernet_hdr_t));
    free(temp_eth_head);

    /* make ip header */
    sr_ip_hdr_t* temp_ip_head = (sr_ip_hdr_t*)(packet + sizeof(sr_ethernet_hdr_t));
    memcpy(temp_ip_head, ip_header, sizeof(sr_ip_hdr_t));
    temp_ip_head->ip_tos = 0;
    temp_ip_head->ip_len = htons(sizeof(sr_ip_hdr_t) + sizeof(sr_icmp_t3_hdr_t));
    temp_ip_head->ip_p = ip_protocol_icmp;
    temp_ip_head->ip_dst = ip_header->ip_src;
    temp_ip_head->ip_src = receiving_if->ip;
    temp_ip_head->ip_ttl = 64;
    temp_ip_head->ip_sum = 0x0000;
    temp_ip_head->ip_sum = cksum(temp_ip_head, sizeof(sr_ip_hdr_t));

    /* make icmp header */
    sr_icmp_t3_hdr_t* icmp_head = (sr_icmp_t3_hdr_t*)(packet + sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t));
    icmp_head->icmp_type = type;
    icmp_head->icmp_code = code;
    memcpy(icmp_head->data, ip_header, ICMP_DATA_SIZE);
    icmp_head->icmp_sum = 0;
    icmp_head->icmp_sum = cksum(icmp_head, sizeof(sr_icmp_t3_hdr_t));

    sr_send_packet(sr, packet, sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t) + sizeof(sr_icmp_t3_hdr_t), interface);
}

void handle_arpreq(struct sr_instance* sr, struct sr_arpreq* req) {
 time_t now = time(NULL);
   if (difftime(now, req->sent) >= 1.0) {
       if (req->times_sent >= 5) {
         struct sr_packet* next = req->packets;
           while (next != NULL) {
               unsigned int len = sizeof(sr_ethernet_hdr_t)+ sizeof(sr_ip_hdr_t) + sizeof(sr_icmp_t3_hdr_t);
               uint8_t* transmit = (uint8_t*)malloc(len);
               send_icmp(sr, transmit, sr_get_interface(sr, next->iface), (sr_ethernet_hdr_t*)transmit, (sr_ip_hdr_t*)(transmit + sizeof(sr_ethernet_hdr_t)), 3, 1, next->iface);
               free(transmit);
               next = next->next;
           }
           sr_arpreq_destroy(&sr->cache, req);
       } else {
           unsigned int len = sizeof(sr_ethernet_hdr_t) + sizeof(sr_arp_hdr_t);
            uint8_t* packet = malloc(len);

            struct sr_if* intrfce = sr_get_interface(sr, req->packets->iface);
            uint8_t null_addr[ETHER_ADDR_LEN] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

            sr_arp_hdr_t* arp_head = (sr_arp_hdr_t*)(packet + sizeof(sr_ethernet_hdr_t));

            arp_head->ar_hrd = htons(arp_hrd_ethernet);
            arp_head->ar_pro = htons(ethertype_ip);
            arp_head->ar_hln = ETHER_ADDR_LEN;
            arp_head->ar_pln = sizeof(uint32_t);

            /* make ARP packet */
            sr_ethernet_hdr_t* ether_head = (sr_ethernet_hdr_t*)packet;
            sr_arp_hdr_t* arp_head2 = (sr_arp_hdr_t*)(packet + sizeof(sr_ethernet_hdr_t));
            uint8_t send_all[ETHER_ADDR_LEN] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
            arp_head->ar_op = htons(arp_op_request);
            memcpy(ether_head->ether_shost, intrfce->addr, ETHER_ADDR_LEN);
            memcpy(ether_head->ether_dhost, send_all, ETHER_ADDR_LEN);
            ether_head->ether_type = (uint16_t)htons(ethertype_arp);

            memcpy(arp_head2->ar_sha, intrfce->addr, ETHER_ADDR_LEN);
            arp_head2->ar_sip = intrfce->ip;
            memcpy(arp_head2->ar_tha, null_addr, ETHER_ADDR_LEN);
            arp_head2->ar_tip = req->ip;

            memcpy(packet, ether_head, sizeof(sr_ethernet_hdr_t));
            memcpy(packet + sizeof(sr_ethernet_hdr_t), arp_head2, sizeof(sr_arp_hdr_t));

            sr_send_packet(sr, packet, len, intrfce->name);
            time(&req->sent);
            req->times_sent++;
            free(packet);
        }
    }
}

void sr_arpcache_sweepreqs(struct sr_instance *sr) {
    struct sr_arpreq* request = sr->cache.requests;

    while (request != NULL) {
        struct sr_arpreq* next = request->next;
        handle_arpreq(sr, request);
        request = next;
    }
} 


void sr_send_arp_request(struct sr_instance* sr, struct sr_arpreq* arp_request) {
	/* malloc */
	uint8_t* temp = malloc(sizeof(sr_arp_hdr_t)+sizeof(sr_ethernet_hdr_t));
	/* search in table */
	struct in_addr ip_search;
	ip_search.s_addr = arp_request->ip;
	
	struct sr_rt* rt_entry = search_rt(sr, ip_search);
	
	if (rt_entry == NULL) {
		sr_arpreq_destroy(&sr->cache, arp_request);
		free(temp);
		return;
	}
	
	struct sr_if* interface = sr_get_interface(sr, rt_entry->interface);
		
    sr_ethernet_hdr_t* ethernet_header = (sr_ethernet_hdr_t*)temp;
	sr_arp_hdr_t* arp_header = (sr_arp_hdr_t*)(temp+sizeof(sr_ethernet_hdr_t));
	
    /* build headers */
	arp_header->ar_hln = 0x06;
	arp_header->ar_hrd = htons(arp_hrd_ethernet);
	arp_header->ar_pln = 0x04;
	arp_header->ar_pro = htons(ethertype_ip);
	arp_header->ar_op = htons(arp_op_request);
	memcpy(arp_header->ar_sha, interface->addr, ETHER_ADDR_LEN);
	memset(arp_header->ar_tha, 0x00, ETHER_ADDR_LEN);
	arp_header->ar_sip = interface->ip;
	
    uint32_t next_ip = 0;
	if (rt_entry->gw.s_addr == 0) {
		next_ip = arp_request->ip;
	} else {
		next_ip = rt_entry->gw.s_addr;
	}
	arp_header->ar_tip = next_ip;

	memcpy(ethernet_header->ether_shost, interface->addr, ETHER_ADDR_LEN);
	memset(ethernet_header->ether_dhost, 0xff, ETHER_ADDR_LEN);
	ethernet_header->ether_type = htons(ethertype_arp);

	sr_send_packet(sr, temp, sizeof(sr_arp_hdr_t)+sizeof(sr_ethernet_hdr_t), iface->name);
	free(temp);
}

/* You should not need to touch the rest of this code. */

/* Checks if an IP->MAC mapping is in the cache. IP is in network byte order.
   You must free the returned structure if it is not NULL. */
struct sr_arpentry *sr_arpcache_lookup(struct sr_arpcache *cache, uint32_t ip) {
    pthread_mutex_lock(&(cache->lock));
    
    struct sr_arpentry *entry = NULL, *copy = NULL;
    
    int i;
    for (i = 0; i < SR_ARPCACHE_SZ; i++) {
        if ((cache->entries[i].valid) && (cache->entries[i].ip == ip)) {
            entry = &(cache->entries[i]);
        }
    }
    
    /* Must return a copy b/c another thread could jump in and modify
       table after we return. */
    if (entry) {
        copy = (struct sr_arpentry *) malloc(sizeof(struct sr_arpentry));
        memcpy(copy, entry, sizeof(struct sr_arpentry));
    }
        
    pthread_mutex_unlock(&(cache->lock));
    
    return copy;
}

/* Adds an ARP request to the ARP request queue. If the request is already on
   the queue, adds the packet to the linked list of packets for this sr_arpreq
   that corresponds to this ARP request. You should free the passed *packet.
   
   A pointer to the ARP request is returned; it should not be freed. The caller
   can remove the ARP request from the queue by calling sr_arpreq_destroy. */
struct sr_arpreq *sr_arpcache_queuereq(struct sr_arpcache *cache,
                                       uint32_t ip,
                                       uint8_t *packet,           /* borrowed */
                                       unsigned int packet_len,
                                       char *iface)
{
    pthread_mutex_lock(&(cache->lock));
    
    struct sr_arpreq *req;
    for (req = cache->requests; req != NULL; req = req->next) {
        if (req->ip == ip) {
            break;
        }
    }
    
    /* If the IP wasn't found, add it */
    if (!req) {
        req = (struct sr_arpreq *) calloc(1, sizeof(struct sr_arpreq));
        req->ip = ip;
        req->next = cache->requests;
        cache->requests = req;
    }
    
    /* Add the packet to the list of packets for this request */
    if (packet && packet_len && iface) {
        struct sr_packet *new_pkt = (struct sr_packet *)malloc(sizeof(struct sr_packet));
        
        new_pkt->buf = (uint8_t *)malloc(packet_len);
        memcpy(new_pkt->buf, packet, packet_len);
        new_pkt->len = packet_len;
		new_pkt->iface = (char *)malloc(sr_IFACE_NAMELEN);
        strncpy(new_pkt->iface, iface, sr_IFACE_NAMELEN);
        new_pkt->next = req->packets;
        req->packets = new_pkt;
    }
    
    pthread_mutex_unlock(&(cache->lock));
    
    return req;
}

/* This method performs two functions:
   1) Looks up this IP in the request queue. If it is found, returns a pointer
      to the sr_arpreq with this IP. Otherwise, returns NULL.
   2) Inserts this IP to MAC mapping in the cache, and marks it valid. */
struct sr_arpreq *sr_arpcache_insert(struct sr_arpcache *cache,
                                     unsigned char *mac,
                                     uint32_t ip)
{
    pthread_mutex_lock(&(cache->lock));
    
    struct sr_arpreq *req, *prev = NULL, *next = NULL; 
    for (req = cache->requests; req != NULL; req = req->next) {
        if (req->ip == ip) {            
            if (prev) {
                next = req->next;
                prev->next = next;
            } 
            else {
                next = req->next;
                cache->requests = next;
            }
            
            break;
        }
        prev = req;
    }
    
    int i;
    for (i = 0; i < SR_ARPCACHE_SZ; i++) {
        if (!(cache->entries[i].valid))
            break;
    }
    
    if (i != SR_ARPCACHE_SZ) {
        memcpy(cache->entries[i].mac, mac, 6);
        cache->entries[i].ip = ip;
        cache->entries[i].added = time(NULL);
        cache->entries[i].valid = 1;
    }
    
    pthread_mutex_unlock(&(cache->lock));
    
    return req;
}

/* Frees all memory associated with this arp request entry. If this arp request
   entry is on the arp request queue, it is removed from the queue. */
void sr_arpreq_destroy(struct sr_arpcache *cache, struct sr_arpreq *entry) {
    pthread_mutex_lock(&(cache->lock));
    
    if (entry) {
        struct sr_arpreq *req, *prev = NULL, *next = NULL; 
        for (req = cache->requests; req != NULL; req = req->next) {
            if (req == entry) {                
                if (prev) {
                    next = req->next;
                    prev->next = next;
                } 
                else {
                    next = req->next;
                    cache->requests = next;
                }
                
                break;
            }
            prev = req;
        }
        
        struct sr_packet *pkt, *nxt;
        
        for (pkt = entry->packets; pkt; pkt = nxt) {
            nxt = pkt->next;
            if (pkt->buf)
                free(pkt->buf);
            if (pkt->iface)
                free(pkt->iface);
            free(pkt);
        }
        
        free(entry);
    }
    
    pthread_mutex_unlock(&(cache->lock));
}

/* Prints out the ARP table. */
void sr_arpcache_dump(struct sr_arpcache *cache) {
    fprintf(stderr, "\nMAC            IP         ADDED                      VALID\n");
    fprintf(stderr, "-----------------------------------------------------------\n");
    
    int i;
    for (i = 0; i < SR_ARPCACHE_SZ; i++) {
        struct sr_arpentry *cur = &(cache->entries[i]);
        unsigned char *mac = cur->mac;
        fprintf(stderr, "%.1x%.1x%.1x%.1x%.1x%.1x   %.8x   %.24s   %d\n", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5], ntohl(cur->ip), ctime(&(cur->added)), cur->valid);
    }
    
    fprintf(stderr, "\n");
}

/* Initialize table + table lock. Returns 0 on success. */
int sr_arpcache_init(struct sr_arpcache *cache) {  
    /* Seed RNG to kick out a random entry if all entries full. */
    srand(time(NULL));
    
    /* Invalidate all entries */
    memset(cache->entries, 0, sizeof(cache->entries));
    cache->requests = NULL;
    
    /* Acquire mutex lock */
    pthread_mutexattr_init(&(cache->attr));
    pthread_mutexattr_settype(&(cache->attr), PTHREAD_MUTEX_RECURSIVE);
    int success = pthread_mutex_init(&(cache->lock), &(cache->attr));
    
    return success;
}

/* Destroys table + table lock. Returns 0 on success. */
int sr_arpcache_destroy(struct sr_arpcache *cache) {
    return pthread_mutex_destroy(&(cache->lock)) && pthread_mutexattr_destroy(&(cache->attr));
}

/* Thread which sweeps through the cache and invalidates entries that were added
   more than SR_ARPCACHE_TO seconds ago. */
void *sr_arpcache_timeout(void *sr_ptr) {
    struct sr_instance *sr = sr_ptr;
    struct sr_arpcache *cache = &(sr->cache);
    
    while (1) {
        sleep(1.0);
        
        pthread_mutex_lock(&(cache->lock));
    
        time_t curtime = time(NULL);
        
        int i;    
        for (i = 0; i < SR_ARPCACHE_SZ; i++) {
            if ((cache->entries[i].valid) && (difftime(curtime,cache->entries[i].added) > SR_ARPCACHE_TO)) {
                cache->entries[i].valid = 0;
            }
        }
        
        sr_arpcache_sweepreqs(sr);

        pthread_mutex_unlock(&(cache->lock));
    }
    
    return NULL;
}
