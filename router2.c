#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

#include "sr_if.h"
#include "sr_rt.h"
#include "sr_router.h"
#include "sr_protocol.h"
#include "sr_arpcache.h"
#include "sr_utils.h"

static const unint32_t BROADCAST = 0xffffffff;
static const uint8_t INFINITY = 16;

void sr_init(struct sr_instance* sr)
{
    /* REQUIRES */
    assert(sr);

    /* Initialize cache and cache cleanup thread */
    sr_arpcache_init(&(sr->cache));

    pthread_attr_init(&(sr->attr));
    pthread_attr_setdetachstate(&(sr->attr), PTHREAD_CREATE_JOINABLE);
    pthread_attr_setscope(&(sr->attr), PTHREAD_SCOPE_SYSTEM);
    pthread_attr_setscope(&(sr->attr), PTHREAD_SCOPE_SYSTEM);
    pthread_t thread;

    pthread_create(&thread, &(sr->attr), sr_arpcache_timeout, sr);
    /* Add initialization code here! */

} /* -- sr_init -- */

/*---------------------------------------------------------------------
 * Method: sr_handlepacket(uint8_t* p,char* interface)
 * Scope:  Global
 *
 * This method is called each time the router receives a packet on the
 * interface.  The packet buffer, the packet length and the receiving
 * interface are passed in as parameters. The packet is complete with
 * ethernet headers.
 *
 * Note: Both the packet buffer and the character's memory are handled
 * by sr_vns_comm.c that means do NOT delete either.  Make a copy of the
 * packet instead if you intend to keep it around beyond the scope of
 * the method call.
 *
 *---------------------------------------------------------------------*/

/* find longest prefix match from interfaces in routing table */
struct sr_rt* longest_prefix(struct sr_rt* rt, struct sr_rt* rt_to_send, uint32_t ip_dst) {
	while (rt != NULL) {
	      if (rt->dest.s_addr == ip_dst) {
	          rt_to_send = rt;
	          break;
	      }
	      rt = rt->next;
	}
	return rt_to_send;
}

/* if ARP packet is request */
void isARPrequest(struct sr_instance* sr, uint8_t * packet, unsigned int len, char* interface, sr_arp_hdr_t* arp_head, struct sr_if* this_interface) {
  while (this_interface != NULL) {
    if (this_interface->ip == arp_head->ar_tip) {
      /* get outgoing packet buffer */
      uint8_t* out_pack = malloc(len);
      memcpy(out_pack, packet, sizeof(sr_ethernet_hdr_t) + sizeof(sr_arp_hdr_t));

      /* make ARP packet for outgoing packet */
      sr_ethernet_hdr_t* ether_head = (sr_ethernet_hdr_t*)out_pack;
      sr_arp_hdr_t* out_head = (sr_arp_hdr_t*)(out_pack + sizeof(sr_ethernet_hdr_t));
      out_head->ar_op = htons(arp_op_reply);
      /* Ethernet header */
      memcpy(ether_head->ether_shost, (uint8_t*)&this_interface->addr, ETHER_ADDR_LEN);
      memcpy(ether_head->ether_dhost, (uint8_t*)&arp_head->ar_sha, ETHER_ADDR_LEN);
      ether_head->ether_type = (uint16_t)htons(ethertype_arp);
      memcpy(out_head->ar_sha, (uint8_t*)&this_interface->addr, ETHER_ADDR_LEN);
      out_head->ar_sip = this_interface->ip;
      memcpy(out_head->ar_tha, (uint8_t*)&arp_head->ar_sha, ETHER_ADDR_LEN);
      out_head->ar_tip = arp_head->ar_sip;

      /* send packet and add to cache */
      sr_send_packet(sr, out_pack, len, interface);
      sr_arpcache_insert(&sr->cache, arp_head->ar_sha, arp_head->ar_sip);

      free(out_pack);
      return;
    }
    this_interface = this_interface->next;
  }
}

/* if ARP packet is reply */
void isARPreply(struct sr_instance* sr, sr_arp_hdr_t* arp_head, struct sr_packet* this_pack, struct sr_if* receiving_if ) {
  while(this_pack != NULL) {
    uint8_t* out_pack = this_pack->buf;
    sr_ethernet_hdr_t* out_head = (sr_ethernet_hdr_t*)out_pack;
    sr_ip_hdr_t* out_ip = (sr_ip_hdr_t*)(out_pack + sizeof(sr_ethernet_hdr_t));
    memcpy(out_head->ether_dhost, arp_head->ar_sha, ETHER_ADDR_LEN);
    memcpy(out_head->ether_shost, receiving_if->addr, ETHER_ADDR_LEN);
    out_ip->ip_ttl = out_ip->ip_ttl - 1;
    out_ip->ip_sum = 0;
    out_ip->ip_sum = cksum(out_ip, sizeof(sr_ip_hdr_t));
    sr_send_packet(sr, out_pack, this_pack->len, receiving_if->name);
    this_pack = this_pack->next;
  } return;
}

/* if packet is ARP */
void isARP(struct sr_instance* sr, uint8_t * packet, unsigned int len, char* interface, sr_arp_hdr_t* arp_head, struct sr_if* receiving_if) {
  if (arp_head->ar_op == htons(arp_op_request)) {
    struct sr_if* intrfce = sr->if_list;
    isARPrequest(sr, packet, len, interface, arp_head, intrfce);
    return;
  }

  if (arp_head->ar_op == htons(arp_op_reply)) {
    struct sr_arpreq* queue_packs = sr_arpcache_insert(&sr->cache, arp_head->ar_sha, arp_head->ar_sip);
    struct sr_packet* this_pack = queue_packs->packets;
    isARPreply(sr, arp_head, this_pack, receiving_if);
    sr_arpreq_destroy(&sr->cache, queue_packs);
    return;
  }
}

void send_icmp (struct sr_instance* sr, uint8_t* packet, struct sr_if* receiving_if, sr_ethernet_hdr_t* eth_head, sr_ip_hdr_t* ip_header, uint8_t type, uint8_t code, char* interface) {
    /* make ether header */
    sr_ethernet_hdr_t* temp_eth_head = malloc(sizeof(sr_ethernet_hdr_t));
    memcpy(temp_eth_head->ether_shost, receiving_if->addr, ETHER_ADDR_LEN);
    memcpy(temp_eth_head->ether_dhost, eth_head->ether_shost, ETHER_ADDR_LEN);
    temp_eth_head->ether_type = (uint16_t)htons(ethertype_ip);
    memcpy(packet, temp_eth_head, sizeof(sr_ethernet_hdr_t));
    free(temp_eth_head);

    /* make ip header */
    sr_ip_hdr_t* temp_ip_head = (sr_ip_hdr_t*)(packet + sizeof(sr_ethernet_hdr_t));
    memcpy(temp_ip_head, ip_header, sizeof(sr_ip_hdr_t));
    temp_ip_head->ip_tos = 0;
    temp_ip_head->ip_len = htons(sizeof(sr_ip_hdr_t) + sizeof(sr_icmp_t3_hdr_t));
    temp_ip_head->ip_p = ip_protocol_icmp;
    temp_ip_head->ip_dst = ip_header->ip_src;
    temp_ip_head->ip_src = receiving_if->ip;
    temp_ip_head->ip_ttl = 64;
    temp_ip_head->ip_sum = 0x0000;
    temp_ip_head->ip_sum = cksum(temp_ip_head, sizeof(sr_ip_hdr_t));

    /* make icmp header */
    sr_icmp_t3_hdr_t* icmp_head = (sr_icmp_t3_hdr_t*)(packet + sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t));
    icmp_head->icmp_type = type;
    icmp_head->icmp_code = code;
    memcpy(icmp_head->data, ip_header, ICMP_DATA_SIZE);
    icmp_head->icmp_sum = 0;
    icmp_head->icmp_sum = cksum(icmp_head, sizeof(sr_icmp_t3_hdr_t));

    sr_send_packet(sr, packet, sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t) + sizeof(sr_icmp_t3_hdr_t), interface);
}

void handle_arpreq(struct sr_instance* sr, struct sr_arpreq* req) {
 time_t now = time(NULL);
   if (difftime(now, req->sent) >= 1.0) {
       if (req->times_sent >= 5) {
         struct sr_packet* next = req->packets;
           while (next != NULL) {
               unsigned int len = sizeof(sr_ethernet_hdr_t)+ sizeof(sr_ip_hdr_t) + sizeof(sr_icmp_t3_hdr_t);
               uint8_t* transmit = (uint8_t*)malloc(len);
               send_icmp(sr, transmit, sr_get_interface(sr, next->iface), (sr_ethernet_hdr_t*)transmit, (sr_ip_hdr_t*)(transmit + sizeof(sr_ethernet_hdr_t)), 3, 1, next->iface);
               free(transmit);
               next = next->next;
           }
           sr_arpreq_destroy(&sr->cache, req);
       } else {
           unsigned int len = sizeof(sr_ethernet_hdr_t) + sizeof(sr_arp_hdr_t);
            uint8_t* packet = malloc(len);

            struct sr_if* intrfce = sr_get_interface(sr, req->packets->iface);
            uint8_t null_addr[ETHER_ADDR_LEN] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

            sr_arp_hdr_t* arp_head = (sr_arp_hdr_t*)(packet + sizeof(sr_ethernet_hdr_t));

            arp_head->ar_hrd = htons(arp_hrd_ethernet);
            arp_head->ar_pro = htons(ethertype_ip);
            arp_head->ar_hln = ETHER_ADDR_LEN;
            arp_head->ar_pln = sizeof(uint32_t);

            /* make ARP packet */
            sr_ethernet_hdr_t* ether_head = (sr_ethernet_hdr_t*)packet;
            sr_arp_hdr_t* arp_head2 = (sr_arp_hdr_t*)(packet + sizeof(sr_ethernet_hdr_t));
            uint8_t send_all[ETHER_ADDR_LEN] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
            arp_head->ar_op = htons(arp_op_request);
            memcpy(ether_head->ether_shost, intrfce->addr, ETHER_ADDR_LEN);
            memcpy(ether_head->ether_dhost, send_all, ETHER_ADDR_LEN);
            ether_head->ether_type = (uint16_t)htons(ethertype_arp);

            memcpy(arp_head2->ar_sha, intrfce->addr, ETHER_ADDR_LEN);
            arp_head2->ar_sip = intrfce->ip;
            memcpy(arp_head2->ar_tha, null_addr, ETHER_ADDR_LEN);
            arp_head2->ar_tip = req->ip;

            memcpy(packet, ether_head, sizeof(sr_ethernet_hdr_t));
            memcpy(packet + sizeof(sr_ethernet_hdr_t), arp_head2, sizeof(sr_arp_hdr_t));

            sr_send_packet(sr, packet, len, intrfce->name);
            time(&req->sent);
            req->times_sent++;
            free(packet);
        }
    }
}

/* check for min len */
int sanity_check(uint8_t* buf, uint32_t len) {
	uint32_t minlength = sizeof(sr_ethernet_hdr_t);

	if (len < minlength) {
		return 0;
	}

	return 1;
}

void echo_req(struct sr_instance* sr, uint8_t* echo_packet, struct sr_if* receiving_if, sr_ip_hdr_t* ip_head, unsigned int len, char* interface, sr_ethernet_hdr_t* ether_head, sr_icmp_hdr_t* icmp_head) {
    sr_ethernet_hdr_t* echo_eth_head = malloc(sizeof(sr_ethernet_hdr_t));
    memcpy(echo_eth_head->ether_shost,  receiving_if->addr, ETHER_ADDR_LEN);
    memcpy(echo_eth_head->ether_dhost, ether_head->ether_shost, ETHER_ADDR_LEN);
    echo_eth_head->ether_type = (uint16_t)htons(ethertype_ip);

    memcpy(echo_packet, echo_eth_head, sizeof(sr_ethernet_hdr_t));
    free(echo_eth_head);

    uint32_t dest_ip = ip_head->ip_dst;
    ip_head->ip_dst = ip_head->ip_src;
    ip_head->ip_src = dest_ip;
    ip_head->ip_sum = 0x0000;
    ip_head->ip_sum = cksum(ip_head, sizeof(sr_ip_hdr_t));
    memcpy(echo_packet + sizeof(sr_ethernet_hdr_t), ip_head, sizeof(sr_ip_hdr_t));

    icmp_head->icmp_type = 0;
    icmp_head->icmp_code = 0;
    icmp_head->icmp_sum = 0;
    icmp_head->icmp_sum = cksum(icmp_head, len - sizeof(sr_ethernet_hdr_t) - sizeof(sr_ip_hdr_t));
    memcpy(echo_packet + sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t), icmp_head, len - sizeof(sr_ethernet_hdr_t) - sizeof(sr_ip_hdr_t));

    sr_send_packet(sr, echo_packet, len, interface);
}

void ttl_exp(struct sr_instance* sr, struct sr_if* receiving_if, sr_ethernet_hdr_t* ether_head, sr_ip_hdr_t* ip_head, char* interface) {
	uint8_t* tosend_t11 = malloc(sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t) + sizeof(sr_icmp_t3_hdr_t));
	memset(tosend_t11, 0, sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t) + sizeof(sr_icmp_t3_hdr_t));
	send_icmp(sr, tosend_t11, receiving_if, ether_head, ip_head, 11, 0, interface);
	free(tosend_t11);
}

void update_head(sr_ip_hdr_t* ip_head) {
	ip_head->ip_ttl = ip_head->ip_ttl - 1;
	ip_head->ip_sum = 0;
	ip_head->ip_sum = cksum(ip_head, sizeof(sr_ip_hdr_t));
}

/* new rt walker */
struct sr_rt* search_rt(struct sr_instance* sr, struct in_addr addr) {

  struct sr_rt* rt_walker = sr->routing_table;
  struct sr_rt* match = NULL;
  uint32_t check = 0x0000;

  while (rt_walker != 0) { 
    if ((addr.s_addr & rt_walker->mask.s_addr) == (rt_walker->dest.s_addr & rt_walker->mask.s_addr)) { 
      if(!match || rt_walker->mask.s_addr >= check) {
        check = rt_walker->mask.s_addr;
        match = rt_walker;
      }
    }
    rt_walker = rt_walker->next;
  }
  return match;
}

/* icmp reply for all types */
int send_icmp_reply(struct sr_instance* sr, uint8_t type, uint8_t code, uint8_t* packet, struct sr_if* interface, unsigned int len) {
	
	sr_ip_hdr_t* ip_head = (sr_ip_hdr_t*) (packet+sizeof(sr_ethernet_hdr_t));
	sr_icmp_hdr_t* icmp_head = 0;
	if (type == 0) {
		icmp_head = (sr_icmp_hdr_t*) (packet+sizeof(sr_ethernet_hdr_t)+sizeof(sr_ip_hdr_t));
	}
	
	unsigned int icmp_len = 0;
	unsigned int tot_len = 0;
	
    if (type == 3 || type == 11) {
			icmp_len = sizeof(sr_icmp_t3_hdr_t)+ htons(ip_head->ip_len);
			tot_len = sizeof(sr_ethernet_hdr_t)+sizeof(sr_ip_hdr_t)+icmp_len;
    } else {
		struct sr_if* ip_check_iface = sr_match_interface(sr, ip_head->ip_dst);
			
		if (sr_obtain_interface_status(sr, ip_check_iface->name)==0) {
			send_icmp_reply(sr, 3, 0, packet, interface, len);
			return 0;
		}
		icmp_len = sizeof(sr_icmp_hdr_t);
		tot_len = len;
    }
	
	uint8_t* temp = (uint8_t*) malloc(tot_len);
	
	if (type == 3 || type == 11) {
		sr_icmp_t3_hdr_t* t3_head = (sr_icmp_t3_hdr_t*) (temp + sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t));
		t3_head->icmp_type = type;
		t3_head->next_mtu = 0;
		t3_head->unused = 0;

		int i;
		for (i = 0; i < ICMP_DATA_SIZE; i++) {
			t3_head->data[i] = *((uint8_t*) ip_head + i);
		}
		
		t3_head->icmp_code = code;
		if (type==0) {
			t3_head->icmp_code = 0;
		}
		t3_head->icmp_sum = 0x0000;
		t3_head->icmp_sum = cksum(t3_head, icmp_len);

	} else {
		
		int j;
		for (j=0; j<len; j++) {
			temp[j] = packet[j];
		}
		
		sr_icmp_hdr_t* icmp_head = (sr_icmp_hdr_t*) (temp + sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t));
		icmp_head->icmp_type = type;
		icmp_head->icmp_code = 0;
		icmp_head->icmp_sum = 0x0000;
		icmp_head->icmp_sum = cksum(icmp_head, len - sizeof(sr_ethernet_hdr_t) - sizeof(sr_ip_hdr_t));
	}

	sr_ethernet_hdr_t* ether_head = (sr_ethernet_hdr_t*)temp;
	sr_ip_hdr_t* ip_head = (sr_ip_hdr_t*)(temp+sizeof(sr_ethernet_hdr_t));
	struct in_addr find_ip;
	find_ip.s_addr = ip_head->ip_src;
	struct sr_rt* routing_table_entry = search_rt(sr, find_ip);
	struct sr_if* iface = sr_get_interface(sr, routing_table_entry->interface);
	
    /* ip header */
    ip_head->ip_tos = 0;
    ip_head->ip_ttl = 64;
    ip_head->ip_p = ip_protocol_icmp;
	ip_head->ip_src = ip_head->ip_dst;
	ip_head->ip_dst = ip_head->ip_src;

  /*populate ip head*/
	if (type != 0) {
		ip_head->ip_hl = 5;
		ip_head->ip_id = htons(ip_head->ip_id)+1;
		ip_head->ip_off = htons(IP_DF);
		ip_head->ip_v = 4;
        ip_head->ip_len = htons(sizeof(sr_ip_hdr_t) + icmp_len);
	} 
	
	ip_head->ip_sum = 0x0000;
	ip_head->ip_sum = cksum(ip_head, sizeof(sr_ip_hdr_t));
	

	/* ethernet header */
	ether_head->ether_type = htons(ethertype_ip);
	uint32_t address_ = 0;
	if (routing_table_entry->gw.s_addr == 0) {
		address_ = ip_head->ip_src;
	} else {
		address_ = routing_table_entry->gw.s_addr;
	}

	struct sr_arpentry* arp_entry = sr_arpcache_lookup(&sr->cache, address_);
	struct sr_if* iface2 = sr_get_interface(sr, iface->name);
	
	    if (arp_entry) { 
	    	memcpy(ether_head->ether_dhost, arp_entry->mac, ETHER_ADDR_LEN);
	    	memcpy(ether_head->ether_shost, iface2->addr, ETHER_ADDR_LEN);
	    	sr_send_packet(sr, temp, tot_len, iface->name);
	    } else {
	    	sr_arpcache_queuereq(&(sr->cache), address_, temp, tot_len, iface2->name); /*i'm assuming that sr_arpcache_sweepreqs handles everything */ 
	}
	return 0;
}

void sr_handlepacket(struct sr_instance* sr,
        uint8_t * packet/* lent */,
        unsigned int len,
        char* interface/* lent */)
{
  /* REQUIRES */
  assert(sr);
  assert(packet);
  assert(interface);

  sr_ethernet_hdr_t* ether_head = (sr_ethernet_hdr_t*)packet;

  /* sanity check packet: min length */
  if (sanity_check(packet, len) == 0) {
	  return;
  }

  struct sr_if* receiving_if = sr_get_interface(sr, interface);

  /* check if ARP or IP */
  if (ether_head->ether_type == htons(ethertype_arp)) {
	  if (ethertype(packet) == ethertype_arp) {
	        sr_arp_hdr_t* arp_head = (sr_arp_hdr_t*)(packet+(sizeof(*ether_head)));
	        isARP(sr, packet, len, interface, arp_head, receiving_if);
	    }
  } else if (ethertype(packet) == ethertype_ip) {
  sr_ip_hdr_t* ip_head = (sr_ip_hdr_t*)(packet + sizeof(sr_ethernet_hdr_t));

  /* if dest is in list of interfaces, send there; if in routing table, send to next hop */
  struct sr_if* if_walker = sr->if_list;

  while (if_walker != NULL) {
	  /* check if the current interface ip is the same as the destination ip */
      if (if_walker->ip == ip_head->ip_dst) {
        /* check which protocol */
  	    /* If the packet is an ICMP echo request and its checksum is valid, send an ICMP echo reply to the sending host. If the packet contains a TCP or UDP payload, send an ICMP port unreachable to the sending host. Otherwise, ignore the packet. Packets destined elsewhere should be forwarded using your normal forwarding logic.*/
        if (ip_head->ip_p == ip_protocol_icmp) {
          sr_icmp_hdr_t* icmp_header = (sr_icmp_hdr_t*)(packet + sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t));

          /* echo request */
          if (icmp_header->icmp_type == 0x08){
              uint8_t* echo_packet = malloc(len);
              /* deal with echo request */
              echo_req(sr, echo_packet, receiving_if, ip_head, len, interface, ether_head, icmp_header);
              free(echo_packet);
              return;
          }
        } else if (ip_head->ip_p == ip_protocol_tcp) {
          /* send port unreachable exception */
          uint8_t* exception_packet = malloc(sizeof(sr_ethernet_hdr_t)+ sizeof(sr_ip_hdr_t) + sizeof(sr_icmp_t3_hdr_t));
          memset(exception_packet, 0, sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t) + sizeof(sr_icmp_t3_hdr_t));
          send_icmp(sr, exception_packet, receiving_if, ether_head, ip_head, 3, 3, interface);
          free(exception_packet);
          return;

        } else if (ip_head->ip_p == ip_protocol_udp) {


            sr_udp_hdr_t* udp_head = (sr_udp_hdr_t*) (packet + sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t));

    		if (udp_head->port_dst == htons(520) && udp_head->port_src == htons(520)) {
    			
    			sr_rip_pkt_t* rip_head = (sr_rip_pkt_t*)(packet + sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t) + sizeof(sr_udp_hdr_t));
    			if (rip_head->command == 1) {
                    /* rip response*/
                    send_rip_response(sr);
    				
    			} else if (rip_head->command == 1){
    				/* update route table */
                    update_route_table(sr, ip_head, rip_head, interface);
    		} else {
    			send_icmp_reply(sr, 3, 3, packet, (struct sr_if*)interface, 0);
    		}
    		
    	} else { /* TCP */
    		send_icmp_reply(sr, 3, 3, packet, (struct sr_if*)interface, 0); /*send an exception is UDP or TCP payload is sent to one of the interfaces*/
    	}





        	/* do stuff with UDP */
        	sr_udp_hdr_t* udp_head = (sr_udp_hdr_t*)(packet + sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t));
        	if (ip_head->ip_dst.s_addr == BROADCAST) { /* check if ip_dst = 255.255.255.255 */
        		if (udp_head->port_src == 0x208 /* 520 */ && udp_head->port_src == 0x208) /* check if UDP source port and dest port are both 520 */{
        			/* if both are 520, this is a RIP packet */
        			sr_rip_hdr_t* rip_head = (sr_rip_hdr_t*)(packet + sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t) + sizeof(sr_udp_hdr_t));
        			if (rip_head->command == 1/* rip request */) {
        				/* send rip response */
        				send_rip_response(sr);
        			} else if (rip_head->command == 2/* rip response */) {
        				/* update routing table */
        				update_routing_table(sr);
        			}
        		} else {
        			/* return ICMP port unreachable packet */
        		}
        	}
        }
      }
      if_walker = if_walker->next;
    }

  	/* ttl exception */
    if (ip_head->ip_ttl <= 1){
    	ttl_exp(sr, receiving_if, ether_head, ip_head, interface);
        return;
    }

    /* find longest prefix match */
    struct sr_rt* rt = sr->routing_table;
    struct sr_rt* rt_to_send = NULL;
    uint32_t ip_dest = ip_head->ip_dst;

    rt_to_send = longest_prefix(rt, rt_to_send, ip_dest);

    if (rt_to_send == NULL) {
        uint8_t* net_unreachable_packet = malloc(sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t) + sizeof(sr_icmp_t3_hdr_t));
        memset(net_unreachable_packet, 0, sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t) + sizeof(sr_icmp_t3_hdr_t));

        send_icmp(sr, net_unreachable_packet, receiving_if, ether_head, ip_head, 3, 0, interface);
        free(net_unreachable_packet);
        return;
    } else { /* forward if it does match an interface */
        struct sr_arpentry* match = sr_arpcache_lookup(&sr->cache, rt_to_send->dest.s_addr);
        struct sr_if* _if = sr_get_interface(sr, rt_to_send->interface);

        if (match == NULL){ /* no match, handle arp req */
          struct sr_arpreq* req = sr_arpcache_queuereq(&sr->cache, rt_to_send->dest.s_addr, packet, len, rt_to_send->interface);
          handle_arpreq(sr, req);
        } else { /* forward IP packet */
          memcpy(ether_head->ether_dhost, match->mac, ETHER_ADDR_LEN);
          memcpy(ether_head->ether_shost, _if->addr, ETHER_ADDR_LEN);
          update_head(ip_head);
          sr_send_packet(sr, packet, len, _if->name);
          free(match);
        }
      }
    }
    /* fill in code here */
}/* end sr_handlepacket */