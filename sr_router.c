/**********************************************************************
 * file:  sr_router.c
 * date:  Mon Feb 18 12:50:42 PST 2002
 * Contact: casado@stanford.edu
 *
 * Description:
 *
 * This file contains all the functions that interact directly
 * with the routing table, as well as the main entry method
 * for routing.
 *
 **********************************************************************/

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>


#include "sr_if.h"
#include "sr_rt.h"
#include "sr_router.h"
#include "sr_protocol.h"
#include "sr_arpcache.h"
#include "sr_utils.h"

void sr_handlearp(struct sr_instance* sr, uint8_t* arp_buffer, char* interface, unsigned int len);
void send_arp_reply(struct sr_instance* sr, sr_arp_hdr_t* arp_packet, char* interface);
void sr_handle_ip(struct sr_instance* sr, uint8_t* packet, char* ip_interface, unsigned int ip_len, unsigned int packet_len);
int send_icmp_exception(struct sr_instance* sr, uint8_t type, uint8_t code, uint8_t* packet, uint8_t* buf, struct sr_if* interface);
int send_icmp_reply(struct sr_instance* sr, uint8_t type, uint8_t code, uint8_t* packet, struct sr_if* interface, unsigned int len);
struct sr_rt* search_rt(struct sr_instance* sr, struct in_addr addr);

/*---------------------------------------------------------------------
 * Method: sr_init(void)
 * Scope:  Global
 *
 * Initialize the routing subsystem
 *
 *---------------------------------------------------------------------*/

void sr_init(struct sr_instance* sr)
{
    /* REQUIRES */
    assert(sr);

    /* Initialize cache and cache cleanup thread */
    sr_arpcache_init(&(sr->cache));

    pthread_attr_init(&(sr->attr));
    pthread_attr_setdetachstate(&(sr->attr), PTHREAD_CREATE_JOINABLE);
    pthread_attr_setscope(&(sr->attr), PTHREAD_SCOPE_SYSTEM);
    pthread_attr_setscope(&(sr->attr), PTHREAD_SCOPE_SYSTEM);
    pthread_t arp;

    pthread_create(&arp, &(sr->attr), sr_arpcache_timeout, sr);

    srand(time(NULL));
    pthread_mutexattr_init(&(sr->rt_lock_attr));
    pthread_mutexattr_settype(&(sr->rt_lock_attr), PTHREAD_MUTEX_RECURSIVE);
    pthread_mutex_init(&(sr->rt_lock), &(sr->rt_lock_attr));

    pthread_attr_setscope(&(sr->rt_attr), PTHREAD_SCOPE_SYSTEM);
    pthread_attr_setscope(&(sr->rt_attr), PTHREAD_SCOPE_SYSTEM);
    pthread_attr_init(&(sr->rt_attr));
    pthread_attr_setdetachstate(&(sr->rt_attr), PTHREAD_CREATE_JOINABLE);
    pthread_t rt;
    pthread_create(&rt, &(sr->rt_attr), sr_rip_timeout, sr);
    /* Add initialization code here! */

}

/* SR HANDLE PACKET - see lab 2 code and build upon */

void sr_handlepacket(struct sr_instance* sr,
        uint8_t * packet/* lent */,
        unsigned int len,
        char* interface/* lent */)
{
  /* REQUIRES */
  assert(sr);
  assert(packet);
  assert(interface);

  /* fill in code here */
  if (len < sizeof(sr_ethernet_hdr_t)) {
  	  return;
  }
  print_hdrs(packet, len);

  if (ethertype(packet) == ethertype_ip) {
      sr_handle_ip(sr, packet, interface, len - sizeof(sr_ethernet_hdr_t), len);
  } else if (ethertype(packet) == ethertype_arp) {
      sr_handlearp(sr, packet + sizeof(sr_ethernet_hdr_t), interface, len - sizeof(sr_ethernet_hdr_t));
  } else {
      return;
  }

}

/* SR HANDLE ARP FUNCTION */

void sr_handlearp(struct sr_instance* sr, uint8_t* arp_buffer, char* iface, unsigned int buffer_length) {
    sr_arp_hdr_t* arp_hdr = (sr_arp_hdr_t*) arp_buffer;
    enum sr_arp_opcode arp_type = (enum sr_arp_opcode) ntohs(arp_hdr->ar_op);

    /* Process ARP based on its type */
    /* first -- no opcode known so return error */
    if(arp_type != arp_op_request && arp_type != arp_op_reply) {
           printf("Don't know arp-opcode ... return\n");
           sr_arpcache_insert(&sr->cache, arp_hdr->ar_sha, arp_hdr->ar_sip);
           return;
       }
    else if (arp_type == arp_op_request) {
        /* Send a reply to the ARP request */
        send_arp_reply(sr, arp_hdr, iface);
    } else if (arp_type == arp_op_reply) {
        printf("Arp reply\n");
        struct sr_arpreq* req_ent = sr_arpcache_insert(&(sr->cache), arp_hdr->ar_sha, arp_hdr->ar_sip);
        if(!req_ent) {
                   printf("no arp request match ...\n");
        }
        else if (req_ent) {
            struct sr_packet* network_packet = req_ent->packets;
            struct sr_arpentry* arp_entry;

            while (network_packet) {
                arp_entry = sr_arpcache_lookup(&(sr->cache), req_ent->ip);

                if (arp_entry) {
                    sr_ethernet_hdr_t* ethernet_hdr = (sr_ethernet_hdr_t*) network_packet->buf;
                    memcpy(ethernet_hdr->ether_dhost, arp_hdr->ar_sha, ETHER_ADDR_LEN);
                    struct sr_if* network_iface = sr_get_interface(sr, iface);
                    memcpy(ethernet_hdr->ether_shost, network_iface->addr, ETHER_ADDR_LEN);
                    ethernet_hdr->ether_type = htons(ethertype_ip);

                    print_hdrs(network_packet->buf, network_packet->len);
                    if (sr_send_packet(sr, network_packet->buf, network_packet->len, network_packet->iface) != 0) {
                        printf("Error sending packet\n");
                    } else {
                        printf("Successful packet send yay\n");
                    }
                    network_packet = network_packet->next;
                } else {
                    printf("Packet sent back to queue\n");
                }
                free(arp_entry);
            }
            sr_arpreq_destroy(&(sr->cache), req_ent);
        }
    }
}

/* SEND_ARP_REPLY */
/* Sends ARP replies when a request is sent. */
void send_arp_reply(struct sr_instance* sr, sr_arp_hdr_t* arp_packet, char* interface) {
	/* Malloc! - dont forget to free*/
	uint8_t* mem_all = (uint8_t*) malloc(sizeof(sr_arp_hdr_t) + sizeof(sr_ethernet_hdr_t));
	sr_arp_hdr_t* arp_head = (sr_arp_hdr_t*)(mem_all+sizeof(sr_ethernet_hdr_t));
	sr_ethernet_hdr_t* eth_head = (sr_ethernet_hdr_t*)mem_all;
	struct sr_if* ifacestruct = (struct sr_if*) interface;
	struct sr_if* interface1 = sr_get_interface(sr, ifacestruct->name);

	/* Ethernet */
	memcpy(eth_head->ether_shost, interface1->addr, ETHER_ADDR_LEN);
	memcpy(eth_head->ether_dhost, arp_packet->ar_sha, ETHER_ADDR_LEN);
	eth_head->ether_type = htons(ethertype_arp);

	/* ARP header: */
	arp_head->ar_pln = sizeof(uint32_t);
	arp_head->ar_pro = htons(0x0800);
	arp_head->ar_sip = interface1->ip;
	arp_head->ar_tip = arp_packet->ar_sip;
	arp_head->ar_op = htons(arp_op_reply);
	arp_head->ar_hrd = htons(arp_hrd_ethernet);
	arp_head->ar_hln = ETHER_ADDR_LEN;
	memcpy(arp_head->ar_sha, interface1->addr, ETHER_ADDR_LEN);
	memcpy(arp_head->ar_tha, arp_packet->ar_sha, ETHER_ADDR_LEN);

	int sanity_check = sr_send_packet(sr, mem_all, sizeof(sr_arp_hdr_t)+sizeof(sr_ethernet_hdr_t), interface1->name);
	print_hdrs(mem_all, sizeof(sr_arp_hdr_t)+sizeof(sr_ethernet_hdr_t));
	if (sanity_check!=0) {
		printf("error!!!\n");
	}
	free(mem_all); /*  free pointer memory sapce !!! */

}


struct sr_if* sr_match_interface(struct sr_instance* sr, uint32_t ip) {
	struct sr_if* match = sr->if_list;
	while(match) {
		if (match->ip == ip) {
			return match;
		}
		match = match->next;
	}
	return 0;
}

/* SR_HANDLE_IP */
/* Handles all IP packets.*/

void sr_handle_ip(struct sr_instance* sr, uint8_t* packet, char* ip_interface, unsigned int ip_len, unsigned int packet_len) {


  /*uint8_t* ip_buffer = packet+sizeof(sr_ethernet_hdr_t);*/
  sr_ip_hdr_t* ip_packet = (sr_ip_hdr_t*) (packet + sizeof(sr_ethernet_hdr_t));


  /* check length */
  if (ip_len < sizeof(sr_ip_hdr_t)) {
	  printf("Packet length too small. Discarding.\n");
	  return;
  }


  /* check checksum */
  uint16_t incoming_checksum = ntohs(ip_packet-> ip_sum);
  ip_packet->ip_sum = 0; /* checksum not included */
  uint16_t calculated_checksum = ntohs(cksum(ip_packet, sizeof(sr_ip_hdr_t)));

  int invalid;

  /* check ttl */
  if (ip_packet->ip_ttl < 1) {
    printf("timed out -- ttl invalid\n");
    send_icmp_reply(sr, 11, 0, packet, (struct sr_if *)ip_interface, 0);
    invalid = 1;
  } else {
    printf("still alive, ttl valid\n");
    invalid = 0;
  }
  /*checksum checker ! */
  if (incoming_checksum != calculated_checksum) {
	  printf("incorrect checksum\n");
	  invalid = 1;
  } else {
	  printf("valid checksum! yay !\n");
	  invalid = 0;
  }

  if(invalid ==1){
	  return;
  }





  /* check if address is within network (sr_if.c/h) <- instance at member if_list */
  /*struct sr_if* interface_check = sr_get_interface(sr, ip_interface->name);*/



  struct sr_if* interface_check = sr_match_interface(sr, ip_packet->ip_dst);


  if (interface_check || (ip_packet->ip_dst == ~(0x0))) { /*in local interface*/



    if (ip_packet->ip_p == ip_protocol_icmp) { /*TO-DO: if ICMP echo request, checksum, then echo reply to the sending host */

    	printf("IP is echo request.\n");

    	send_icmp_reply(sr, 0, 9, packet, (struct sr_if*)ip_interface, packet_len);
    }
    else {
    	/* check protocol of ip packet */
    	uint8_t incoming_ip_proto = ip_packet->ip_p;
    	if (incoming_ip_proto == ip_protocol_udp) {
    		printf("Is UDP packet.\n");

    		sr_udp_hdr_t* incoming_udp_packet = (sr_udp_hdr_t*) (packet + sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t));

    		/* TODO: Is packet RIP or other UDP? */
    		uint16_t port_dst = incoming_udp_packet->port_dst;
    		uint16_t port_src = incoming_udp_packet->port_src;
    		printf("Des port: %d\nSrc Port: %d\n", htons(port_dst), htons(port_src));
    		if (port_dst == htons(520) && port_src == htons(520)) {
    			printf("Is RIP packet.\n");

    			sr_rip_pkt_t* incoming_rip_packet = (sr_rip_pkt_t*)(packet + sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t) + sizeof(sr_udp_hdr_t));
    			uint8_t command = incoming_rip_packet->command;
    			printf("Command: %d\n", command);
    			if (command == 2) {
    				printf("RIP response.\n");
    				update_route_table(sr, ip_packet, incoming_rip_packet, ip_interface);
    				/* TODO: implement update_route_table */

    			} else if (command == 1){
    				printf("RIP request.\n");
    				send_rip_response(sr);
    				/* TODO: implement send RIP response */
    			} else {
    				printf("Unknown command.\n");
    				return;
    			}
    		} else {
    			printf("Is not RIP packet, sending exception.\n");
    			send_icmp_reply(sr, 3, 3, packet, (struct sr_if*)ip_interface, 0);
    		}

    	} else {
    		printf("Is TCP, sending exception.\n");
    		send_icmp_reply(sr, 3, 3, packet, (struct sr_if*)ip_interface, 0); /*send an exception is UDP or TCP payload is sent to one of the interfaces*/
    	}


    }
  }
  /*if not within network/destined elsewhere*/
  else {
	  if (ip_packet->ip_ttl < 2) {
	    	  printf("Packet timed out. TTL <= 1. \n");
	    	  send_icmp_reply(sr, 11, 0, packet, (struct sr_if *)ip_interface, 0); /*time exceeded*/
	    	  return;
	  }
	  uint8_t* fwd_packet = (uint8_t*)malloc(packet_len);
	  int j;
	  for (j = 0; j < packet_len; j++) {
		  fwd_packet[j] = packet[j];
	  }

	  ip_packet = (sr_ip_hdr_t*) (fwd_packet + sizeof(sr_ethernet_hdr_t));
	  ip_packet->ip_ttl = ip_packet->ip_ttl - 1;
	  ip_packet->ip_sum = 0;
	  ip_packet->ip_sum = cksum((uint8_t*)ip_packet, sizeof(sr_ip_hdr_t));
      /*find out which entry in the routing table has the longest prefix match with the destination IP address*/
      printf("Loading routing table from server.\n");
      struct in_addr ip_check;
      ip_check.s_addr = ip_packet->ip_dst;
      struct sr_rt* next_hop_ip = search_rt(sr, ip_check);

      if (next_hop_ip == 0 || (next_hop_ip->metric == htons(INFINITY))) {
        printf("Next hop not found.\n");
        send_icmp_reply(sr, 3, 0, packet, (struct sr_if*)ip_interface, 0); /*port unreachable*/
        return; /*discard packet*/
      }
      sr_print_routing_entry(next_hop_ip);
      struct sr_if* next_hop_interface = sr_get_interface(sr, next_hop_ip->interface);
      /*check arp cache for the next MAC address corresponding to the next-hop IP */
      printf("Searching for next hop MAC address.\n");
      uint32_t nh_addr = 0;
      if (next_hop_ip->gw.s_addr == 0) {
    	  nh_addr = ip_packet->ip_dst;
    	  printf("ip dst used\n");
      } else {
    	  nh_addr = next_hop_ip->gw.s_addr;
    	  printf("gw used\n");
      }
      printf("next hop ip: %d\n", nh_addr);

      sr_print_routing_table(sr);

      struct sr_arpentry* entry = sr_arpcache_lookup(&sr->cache, nh_addr);
      if (entry) { /* found entry */
    	  printf("Entry found. Forwarding packet.\n");
    	  sr_ethernet_hdr_t* ethernet_header = (sr_ethernet_hdr_t*) fwd_packet;
    	  memcpy(ethernet_header->ether_dhost, entry->mac, ETHER_ADDR_LEN);
    	  memcpy(ethernet_header->ether_shost, next_hop_interface->addr, ETHER_ADDR_LEN);

    	  int success = sr_send_packet(sr, fwd_packet, packet_len, next_hop_ip->interface);
    	  printf("FORWARDING ICMP PACKET\n\n\n");
    	  print_hdrs(fwd_packet, packet_len);
    	  if (success == 0) {
    		  printf("Forwarded successfully.");
    	  } else {
    		  printf("Error in forwarding packet.");
    	  }
      } else {
    	  printf("No entry found. Adding this packet to queue:\n");
    	  printf("Next hop ip:%d\n", nh_addr);
    	  print_hdr_ip((uint8_t*)ip_packet);
    	  sr_arpcache_queuereq(&sr->cache, nh_addr, fwd_packet, packet_len, next_hop_ip->interface); /*i'm assuming that sr_arpcache_sweepreqs handles everything */
      }
  }
}

/* SEARCH_RT */
struct sr_rt* search_rt(struct sr_instance* sr, struct in_addr addy) {

  struct sr_rt* highest = NULL;

  struct sr_rt* current = sr->routing_table;

  uint32_t match_check = 0;

  while (current) {
    if ((current->dest.s_addr & current->mask.s_addr) == (addy.s_addr & current->mask.s_addr)) {
      if(!highest || (current->mask.s_addr > match_check) || (current->mask.s_addr == match_check)) {
        highest = current;
		match_check = current->mask.s_addr;
      }
    }
    current = current->next;
  }
  return highest;
}
int send_icmp_reply(struct sr_instance* sr, uint8_t type, uint8_t code, uint8_t* packet, struct sr_if* interface, unsigned int len) {
	
    sr_ip_hdr_t* in_ip_head = (sr_ip_hdr_t*) (packet+sizeof(sr_ethernet_hdr_t));
	sr_icmp_hdr_t* in_icmp_head = 0;
	if (type == 0) {
		in_icmp_head = (sr_icmp_hdr_t*) (packet+sizeof(sr_ethernet_hdr_t)+sizeof(sr_ip_hdr_t));
	}
	
	
	
	unsigned int icmp_len = 0;
	unsigned int tot_len = 0;
	if (type == 3 || type == 11) {
		icmp_len = sizeof(sr_icmp_t3_hdr_t)+ htons(in_ip_head->ip_len);
		tot_len = sizeof(sr_ethernet_hdr_t)+sizeof(sr_ip_hdr_t)+icmp_len;
	} else {
		struct sr_if* check_ip_interface = sr_match_interface(sr, in_ip_head->ip_dst);
			
		printf("interface: %s\niface status:%d\n", check_ip_interface->name, (sr_obtain_interface_status(sr, check_ip_interface->name)));
		if (sr_obtain_interface_status(sr, check_ip_interface->name)==0) {
			send_icmp_reply(sr, 3, 0, packet, interface, len);
			return 0;
		}
		icmp_len = sizeof(sr_icmp_hdr_t);
		tot_len = len;
	}
	
	uint8_t* temp = (uint8_t*) malloc(tot_len);

	if (type == 3 || type == 11) {
		sr_icmp_t3_hdr_t* icmp_t3_hdr = (sr_icmp_t3_hdr_t*) (temp + sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t));
		icmp_t3_hdr->icmp_type = type;
		icmp_t3_hdr->next_mtu = 0;
		icmp_t3_hdr->unused = 0;

		
		int i;
		for (i = 0; i < ICMP_DATA_SIZE; i++) {
			icmp_t3_hdr->data[i] = *((uint8_t*) in_ip_head + i);
		}

		
		icmp_t3_hdr->icmp_code = code;
		if (type==0) {
			icmp_t3_hdr->icmp_code = 0;
		}
		icmp_t3_hdr->icmp_sum = 0;
		icmp_t3_hdr->icmp_sum = cksum(icmp_t3_hdr, icmp_len);

	} else {
		
		int j;
		for (j=0; j<len; j++) {
			temp[j] = packet[j];
		}
		
		sr_icmp_hdr_t* icmp_head = (sr_icmp_hdr_t*) (temp + sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t));
		icmp_head->icmp_type = type;
		icmp_head->icmp_code = 0;
		icmp_head->icmp_sum = 0x0000;
		icmp_head->icmp_sum = cksum(icmp_head, len - sizeof(sr_ethernet_hdr_t) - sizeof(sr_ip_hdr_t));
	}
	
	sr_ethernet_hdr_t* ether_head = (sr_ethernet_hdr_t*)temp;
	sr_ip_hdr_t* ip_head = (sr_ip_hdr_t*)(temp+sizeof(sr_ethernet_hdr_t));
	struct in_addr ip_check;
	ip_check.s_addr = in_ip_head->ip_src;
	struct sr_rt* rt_entry = search_rt(sr, ip_check);
	struct sr_if* iface = sr_get_interface(sr, rt_entry->interface);
	
		

    /* ip head*/
    ip_head->ip_tos = 0;
	ip_head->ip_ttl = 64;
	ip_head->ip_p = ip_protocol_icmp;
	ip_head->ip_src = in_ip_head->ip_dst;
	ip_head->ip_dst = in_ip_head->ip_src;


	if (type != 0) {
		ip_head->ip_hl = 5;
		ip_head->ip_id = htons(in_ip_head->ip_id)+1;
		ip_head->ip_off = htons(IP_DF);
		ip_head->ip_v = 4;
		ip_head->ip_len = htons(sizeof(sr_ip_hdr_t) + icmp_len);
	}
	
	
	ip_head->ip_sum = 0x0000;
	ip_head->ip_sum = cksum(ip_head, sizeof(sr_ip_hdr_t));
	

	/* ethernet header */
	
	ether_head->ether_type = htons(ethertype_ip);
	uint32_t nh_addr = 0;
	if (rt_entry->gw.s_addr == 0) {
		nh_addr = in_ip_head->ip_src;
	} else {
		nh_addr = rt_entry->gw.s_addr;
	}
	struct sr_arpentry* entry = sr_arpcache_lookup(&sr->cache, nh_addr);
	struct sr_if* iface2 = sr_get_interface(sr, iface->name);
	
	      if (entry != NULL) { 
	    	  
	    	  memcpy(ether_head->ether_dhost, entry->mac, ETHER_ADDR_LEN);
	    	  memcpy(ether_head->ether_shost, iface2->addr, ETHER_ADDR_LEN);
	    	  print_hdrs(temp, sizeof(sr_ethernet_hdr_t)+ntohs(in_ip_head->ip_len));
	    	  sr_send_packet(sr, temp, tot_len, iface->name);
	      } else {
	    	  print_hdrs(temp, tot_len);
	    	  sr_arpcache_queuereq(&(sr->cache), nh_addr, temp, tot_len, iface2->name); /*i'm assuming that sr_arpcache_sweepreqs handles everything */ 
	}
	return 0;
}



