int send_icmp_reply(struct sr_instance* sr, uint8_t type, uint8_t code, uint8_t* packet, struct sr_if* interface, unsigned int len) {
	
    sr_ip_hdr_t* in_ip_head = (sr_ip_hdr_t*) (packet+sizeof(sr_ethernet_hdr_t));
	sr_icmp_hdr_t* in_icmp_head = 0;
	if (type == 0) {
		in_icmp_head = (sr_icmp_hdr_t*) (packet+sizeof(sr_ethernet_hdr_t)+sizeof(sr_ip_hdr_t));
	}
	
	
	
	unsigned int icmp_len = 0;
	unsigned int tot_len = 0;
	if (type == 3 || type == 11) {
		icmp_len = sizeof(sr_icmp_t3_hdr_t)+ htons(in_ip_head->ip_len);
		tot_len = sizeof(sr_ethernet_hdr_t)+sizeof(sr_ip_hdr_t)+icmp_len;
	} else {
		struct sr_if* check_ip_interface = sr_match_interface(sr, in_ip_head->ip_dst);
			
		printf("interface: %s\niface status:%d\n", check_ip_interface->name, (sr_obtain_interface_status(sr, check_ip_interface->name)));
		if (sr_obtain_interface_status(sr, check_ip_interface->name)==0) {
			send_icmp_reply(sr, 3, 0, packet, interface, len);
			return 0;
		}
		icmp_len = sizeof(sr_icmp_hdr_t);
		tot_len = len;
	}
	
	uint8_t* temp = (uint8_t*) malloc(tot_len);

	if (type == 3 || type == 11) {
		sr_icmp_t3_hdr_t* icmp_t3_hdr = (sr_icmp_t3_hdr_t*) (temp + sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t));
		icmp_t3_hdr->icmp_type = type;
		icmp_t3_hdr->next_mtu = 0;
		icmp_t3_hdr->unused = 0;

		
		int i;
		for (i = 0; i < ICMP_DATA_SIZE; i++) {
			icmp_t3_hdr->data[i] = *((uint8_t*) in_ip_head + i);
		}

		
		icmp_t3_hdr->icmp_code = code;
		if (type==0) {
			icmp_t3_hdr->icmp_code = 0;
		}
		icmp_t3_hdr->icmp_sum = 0;
		icmp_t3_hdr->icmp_sum = cksum(icmp_t3_hdr, icmp_len);

	} else {
		
		int j;
		for (j=0; j<len; j++) {
			temp[j] = packet[j];
		}
		
		sr_icmp_hdr_t* icmp_head = (sr_icmp_hdr_t*) (temp + sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t));
		icmp_head->icmp_type = type;
		icmp_head->icmp_code = 0;
		icmp_head->icmp_sum = 0x0000;
		icmp_head->icmp_sum = cksum(icmp_head, len - sizeof(sr_ethernet_hdr_t) - sizeof(sr_ip_hdr_t));
	}
	
	sr_ethernet_hdr_t* ether_head = (sr_ethernet_hdr_t*)temp;
	sr_ip_hdr_t* ip_head = (sr_ip_hdr_t*)(temp+sizeof(sr_ethernet_hdr_t));
	struct in_addr ip_check;
	ip_check.s_addr = in_ip_head->ip_src;
	struct sr_rt* rt_entry = search_rt(sr, ip_check);
	struct sr_if* iface = sr_get_interface(sr, rt_entry->interface);
	
		

    /* ip head*/
    ip_head->ip_tos = 0;
	ip_head->ip_ttl = 64;
	ip_head->ip_p = ip_protocol_icmp;
	ip_head->ip_src = in_ip_head->ip_dst;
	ip_head->ip_dst = in_ip_head->ip_src;


	if (type != 0) {
		ip_head->ip_hl = 5;
		ip_head->ip_id = htons(in_ip_head->ip_id)+1;
		ip_head->ip_off = htons(IP_DF);
		ip_head->ip_v = 4;
		ip_head->ip_len = htons(sizeof(sr_ip_hdr_t) + icmp_len);
	}
	
	
	ip_head->ip_sum = 0x0000;
	ip_head->ip_sum = cksum(ip_head, sizeof(sr_ip_hdr_t));
	

	/* ethernet header */
	
	ether_head->ether_type = htons(ethertype_ip);
	uint32_t nh_addr = 0;
	if (rt_entry->gw.s_addr == 0) {
		nh_addr = in_ip_head->ip_src;
	} else {
		nh_addr = rt_entry->gw.s_addr;
	}
	struct sr_arpentry* entry = sr_arpcache_lookup(&sr->cache, nh_addr);
	struct sr_if* iface2 = sr_get_interface(sr, iface->name);
	
	      if (entry != NULL) { 
	    	  
	    	  memcpy(ether_head->ether_dhost, entry->mac, ETHER_ADDR_LEN);
	    	  memcpy(ether_head->ether_shost, iface2->addr, ETHER_ADDR_LEN);
	    	  print_hdrs(temp, sizeof(sr_ethernet_hdr_t)+ntohs(in_ip_head->ip_len));
	    	  sr_send_packet(sr, temp, tot_len, iface->name);
	      } else {
	    	  print_hdrs(temp, tot_len);
	    	  sr_arpcache_queuereq(&(sr->cache), nh_addr, temp, tot_len, iface2->name); /*i'm assuming that sr_arpcache_sweepreqs handles everything */ 
	}
	return 0;
}